<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211121163102 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE treasure_hunt_arg (id INT AUTO_INCREMENT NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE treasure_hunt_arg_part (id INT AUTO_INCREMENT NOT NULL, treasure_hunt_arg_id INT DEFAULT NULL, answer VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, sequence INT NOT NULL, INDEX IDX_98EEA17ECAFD8AE (treasure_hunt_arg_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE treasure_hunt_arg_part ADD CONSTRAINT FK_98EEA17ECAFD8AE FOREIGN KEY (treasure_hunt_arg_id) REFERENCES treasure_hunt_arg (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE treasure_hunt_arg_part DROP FOREIGN KEY FK_98EEA17ECAFD8AE');
        $this->addSql('DROP TABLE treasure_hunt_arg');
        $this->addSql('DROP TABLE treasure_hunt_arg_part');
    }
}
