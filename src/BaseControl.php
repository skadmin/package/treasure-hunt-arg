<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'treasure-hunt-arg';
    public const DIR_IMAGE = 'treasure-hunt-arg';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-treasure-chest']),
            'items'   => ['overview'],
        ]);
    }
}
