<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArg;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArgPart\TreasureHuntArgPart;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TreasureHuntArg
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;

    /** @var ArrayCollection|Collection|TreasureHuntArgPart[] */
    #[ORM\OneToMany(targetEntity: TreasureHuntArgPart::class, mappedBy: 'treasureHuntArg')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    private $treasureHuntArgParts;

    public function __construct()
    {
        $this->treasureHuntArgParts = new ArrayCollection();
    }

    public function update(string $name, string $content, string $webalize): void
    {
        $this->name     = $name;
        $this->content  = $content;
        $this->webalize = $webalize;
    }

    /**
     * @return ArrayCollection|Collection|TreasureHuntArgPart[]
     */
    public function getTreasureHuntArgParts()
    {
        return $this->treasureHuntArgParts;
    }
}
