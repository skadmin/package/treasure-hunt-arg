<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArg;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits;
use SkadminUtils\DoctrineTraits\Facade;

final class TreasureHuntArgFacade extends Facade
{
    use DoctrineTraits\Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TreasureHuntArg::class;
    }

    public function create(string $name, string $content, string $webalize): TreasureHuntArg
    {
        return $this->update(null, $name, $content, $webalize);
    }

    public function update(?int $id, string $name, string $content, string $webalize): TreasureHuntArg
    {
        if ($webalize === '') {
            $webalize = $this->getValidWebalize($name);
        }

        $treasureHuntArg = $this->get($id);
        $treasureHuntArg->update($name, $content, $webalize);

        $this->em->persist($treasureHuntArg);
        $this->em->flush();

        return $treasureHuntArg;
    }

    public function get(?int $id = null): TreasureHuntArg
    {
        if ($id === null) {
            return new TreasureHuntArg();
        }

        $treasureHuntArg = parent::get($id);

        if ($treasureHuntArg === null) {
            return new TreasureHuntArg();
        }

        return $treasureHuntArg;
    }

    public function findByWebalize(string $webalize): ?TreasureHuntArg
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
