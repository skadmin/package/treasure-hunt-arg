<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArgPart;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArg\TreasureHuntArg;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;

final class TreasureHuntArgPartFacade extends Facade
{
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TreasureHuntArgPart::class;
    }

    public function create(TreasureHuntArg $treasureHuntArg, string $name, string $content, string $answer, bool $withoutAnswer): TreasureHuntArgPart
    {
        $treasureHuntArgPart = $this->get();
        $treasureHuntArgPart->create($treasureHuntArg, $name, $content, $answer, $withoutAnswer);

        $treasureHuntArgPart->setSequence($this->getValidSequence());

        $this->em->persist($treasureHuntArgPart);
        $this->em->flush();

        return $treasureHuntArgPart;
    }

    public function update(?int $id, string $name, string $content, string $answer, bool $withoutAnswer): TreasureHuntArgPart
    {
        $treasureHuntArgPart = $this->get($id);
        $treasureHuntArgPart->update($name, $content, $answer, $withoutAnswer);

        $this->em->persist($treasureHuntArgPart);
        $this->em->flush();

        return $treasureHuntArgPart;
    }

    public function get(?int $id = null): TreasureHuntArgPart
    {
        if ($id === null) {
            return new TreasureHuntArgPart();
        }

        $treasureHuntArgPart = parent::get($id);

        if ($treasureHuntArgPart === null) {
            return new TreasureHuntArgPart();
        }

        return $treasureHuntArgPart;
    }

    /**
     * @return TreasureHuntArgPart[]
     */
    public function getAll(): array
    {
        return $this->em
            ->getRepository($this->table)
            ->findAll();
    }

    public function getModelForTreasureHuntArg(TreasureHuntArg $treasureHuntArg): QueryBuilder
    {
        $repository = $this->em->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('treasureHuntArg', $treasureHuntArg));

        return $repository->createQueryBuilder('a')
            ->orderBy('a.sequence')
            ->addCriteria($criteria);
    }
}
