<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArgPart;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArg\TreasureHuntArg;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TreasureHuntArgPart
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Content;
    use Entity\Sequence;

    #[ORM\Column]
    private string $answer;

    #[ORM\Column(options: ['default' => true])]
    private bool $withoutAnswer = false;

    #[ORM\ManyToOne(targetEntity: TreasureHuntArg::class, inversedBy: 'treasureHuntArgParts')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private TreasureHuntArg $treasureHuntArg;

    public function create(TreasureHuntArg $treasureHuntArg, string $name, string $content, string $answer, bool $withoutAnswer): void
    {
        $this->treasureHuntArg = $treasureHuntArg;
        $this->update($name, $content, $answer, $withoutAnswer);
    }

    public function update(string $name, string $content, string $answer, bool $withoutAnswer): void
    {
        $this->name          = $name;
        $this->content       = $content;
        $this->answer        = $answer;
        $this->withoutAnswer = $withoutAnswer;
    }

    public function getAnswer(): string
    {
        return $this->answer;
    }

    public function isWithoutAnswer(): bool
    {
        return $this->withoutAnswer;
    }

    public function getTreasureHuntArg(): TreasureHuntArg
    {
        return $this->treasureHuntArg;
    }
}
