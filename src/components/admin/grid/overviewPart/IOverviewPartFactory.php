<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg\Components\Admin;

interface IOverviewPartFactory
{
    public function create(int $id): OverviewPart;
}
