<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Nette\Utils\Strings;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use Skadmin\TreasureHuntArg\BaseControl;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArg\TreasureHuntArg;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArg\TreasureHuntArgFacade;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArgPart\TreasureHuntArgPart;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArgPart\TreasureHuntArgPartFacade;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function str_replace;
use function strip_tags;

class OverviewPart extends GridControl
{
    use APackageControl;

    private TreasureHuntArgPartFacade $facade;
    private TreasureHuntArgFacade     $facadeTreasureHuntArg;
    private TreasureHuntArg           $treasureHuntArg;
    private LoaderFactory             $webLoader;

    public function __construct(int $id, TreasureHuntArgPartFacade $facade, TreasureHuntArgFacade $facadeTreasureHuntArg, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade                = $facade;
        $this->facadeTreasureHuntArg = $facadeTreasureHuntArg;
        $this->webLoader             = $webLoader;

        $this->treasureHuntArg = $this->facadeTreasureHuntArg->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewPart.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'treasure-hunt-arg-part.overview.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('jQueryUi'),
        ];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        // DATA
        $replacementDialYesNo = Arrays::map(Constant::DIAL_YES_NO, fn ($value): string => $this->translator->translate($value));

        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelForTreasureHuntArg($this->treasureHuntArg));

        // COLUMNS
        $grid->addColumnText('name', 'grid.treasure-hunt-arg-part.overview.name')
            ->setRenderer(function (TreasureHuntArgPart $treasureHuntArgPart): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit-part',
                        'id'      => $treasureHuntArgPart->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($treasureHuntArgPart->getName());

                return $name;
            });
        $grid->addColumnText('content', 'grid.treasure-hunt-arg-part.overview.content')
            ->setRenderer(static fn (TreasureHuntArgPart $thargp): string => Strings::truncate(strip_tags($thargp->getContent()), 100));
        $grid->addColumnText('withoutAnswer', 'grid.treasure-hunt-arg-part.overview.without-answer')
            ->setReplacement($replacementDialYesNo)
            ->setAlign('center');
        $grid->addColumnText('answer', 'grid.treasure-hunt-arg-part.overview.answer')
            ->setRenderer(static fn (TreasureHuntArgPart $thargp): Html => (new Html())->setHtml($thargp->isWithoutAnswer() ? '/' : str_replace(';', '<br>', $thargp->getAnswer())))
            ->setAlign('center');

        // FILTER
        $grid->addFilterText('name', 'grid.treasure-hunt-arg-part.overview.name');
        $grid->addFilterText('content', 'grid.treasure-hunt-arg-part.overview.content');
        $grid->addFilterText('answer', 'grid.treasure-hunt-arg-part.overview.answer');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('edit', 'grid.treasure-hunt-arg-part.overview.action.edit', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render'  => 'edit-part',
                ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default', 'grid.treasure-hunt-arg-part.overview-regitration.action.overview', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default#1', 'grid.treasure-hunt-arg-part.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit-part',
                'thargId' => $this->treasureHuntArg->getId(),
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-primary');
        }

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.treasure-hunt-arg-part.overview.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
