<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg\Components\Admin;

use App\Model\System\APackageControl;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use Skadmin\TreasureHuntArg\BaseControl;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArg\TreasureHuntArg;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArg\TreasureHuntArgFacade;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

use function str_replace;

class Overview extends GridControl
{
    use APackageControl;

    private TreasureHuntArgFacade $facade;

    public function __construct(TreasureHuntArgFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'treasure-hunt-arg.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('name', 'grid.treasure-hunt-arg.overview.name')
            ->setRenderer(function (TreasureHuntArg $treasureHuntArg): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $treasureHuntArg->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $href = $this->getPresenter()->link('//:Front:TreasureHuntArg:default', ['tharg' => $treasureHuntArg->getWebalize()]);

                $link = Html::el('code', ['class' => 'text-muted d-block small text-nowrap'])
                    ->setText(str_replace(['https://', 'http://', 'www.'], '', $href));

                $name->setText($treasureHuntArg->getName());

                $render = new Html();
                $render->addHtml($name);
                $render->addHtml($link);

                return $render;
            });
        $grid->addColumnText('partCount', 'grid.treasure-hunt-arg.overview.part-count')
            ->setRenderer(static fn (TreasureHuntArg $tharg): int => $tharg->getTreasureHuntArgParts()->count())
            ->setAlign('center');

        // FILTER
        $grid->addFilterText('name', 'grid.treasure-hunt-arg.overview.name', ['name', 'webalize', 'content']);

        // OTHER
        $grid->setDefaultSort(['id' => 'DESC']);

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $grid->addAction('overview-part', 'grid.treasure-hunt-arg.overview.action.overview-part', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render'  => 'overview-part',
                ])->setIcon('list')
                ->setClass('btn btn-xs btn-outline-primary');
        }

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('edit', 'grid.treasure-hunt-arg.overview.overview.action.edit', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render'  => 'edit',
                ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');

            $grid->addToolbarButton('Component:default', 'grid.treasure-hunt-arg.overview.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-primary');
        }

        return $grid;
    }
}
