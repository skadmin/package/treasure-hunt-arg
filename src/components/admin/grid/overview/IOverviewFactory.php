<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
