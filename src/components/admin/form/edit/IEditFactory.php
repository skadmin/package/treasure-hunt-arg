<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg\Components\Admin;

interface IEditFactory
{
    public function create(?int $id = null): Edit;
}
