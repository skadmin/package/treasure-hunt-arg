<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use Skadmin\TreasureHuntArg\BaseControl;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArg\TreasureHuntArg;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArg\TreasureHuntArgFacade;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory         $webLoader;
    private TreasureHuntArgFacade $facade;
    private TreasureHuntArg       $treasureHuntArg;

    public function __construct(?int $id, TreasureHuntArgFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->treasureHuntArg = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->treasureHuntArg->isLoaded()) {
            return new SimpleTranslation('treasure-hunt-arg.edit.title - %s', $this->treasureHuntArg->getName());
        }

        return 'treasure-hunt-arg.edit.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.treasure-hunt-arg.edit.name')
            ->setRequired('form.treasure-hunt-arg.edit.name.req');
        $form->addText('webalize', 'form.treasure-hunt-arg.edit.webalize')
            ->setHtmlAttribute('placeholder', 'form.treasure-hunt-arg.edit.webalize.placeholder');
        $form->addTextArea('content', 'form.treasure-hunt-arg.edit.content');

        // BUTTON
        $form->addSubmit('send', 'form.treasure-hunt-arg.edit.send');
        $form->addSubmit('send_back', 'form.treasure-hunt-arg.edit.send-back');
        $form->addSubmit('back', 'form.treasure-hunt-arg.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->treasureHuntArg->isLoaded()) {
            return [];
        }

        return [
            'name'     => $this->treasureHuntArg->getName(),
            'content'  => $this->treasureHuntArg->getContent(),
            'webalize' => $this->treasureHuntArg->getWebalize(),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->treasureHuntArg->isLoaded()) {
            $treasureHuntArg = $this->facade->update(
                $this->treasureHuntArg->getId(),
                $values->name,
                $values->content,
                $values->webalize
            );
            $this->onFlashmessage('form.treasure-hunt-arg.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $treasureHuntArg = $this->facade->create(
                $values->name,
                $values->content,
                $values->webalize
            );
            $this->onFlashmessage('form.treasure-hunt-arg.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $treasureHuntArg->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
            'id'      => $this->treasureHuntArg->getId(),
        ]);
    }
}
