<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use Skadmin\TreasureHuntArg\BaseControl;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArg\TreasureHuntArgFacade;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArgPart\TreasureHuntArgPart;
use Skadmin\TreasureHuntArg\Doctrine\TreasureHuntArgPart\TreasureHuntArgPartFacade;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function intval;

class EditPart extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory             $webLoader;
    private TreasureHuntArgPartFacade $facade;
    private TreasureHuntArgFacade     $facadeTreasureHuntArg;
    private TreasureHuntArgPart       $treasureHuntArgPart;

    public function __construct(?int $id, TreasureHuntArgPartFacade $facade, TreasureHuntArgFacade $facadeTreasureHuntArg, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade                = $facade;
        $this->facadeTreasureHuntArg = $facadeTreasureHuntArg;
        $this->webLoader             = $webLoader;

        $this->treasureHuntArgPart = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->treasureHuntArgPart->isLoaded()) {
            return new SimpleTranslation('treasure-hunt-arg.edit-part.title - %s', $this->treasureHuntArgPart->getName());
        }

        return 'treasure-hunt-arg.edit-part.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editPart.latte');

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addHidden('thargId', $this->getPresenter()->getParameter('thargId'));
        $form->addText('name', 'form.treasure-hunt-arg.edit-part.name')
            ->setRequired('form.treasure-hunt-arg.edit-part.name.req');
        $form->addTextArea('content', 'form.treasure-hunt-arg.edit-part.content');
        $form->addCheckbox('withoutAnswer', 'form.treasure-hunt-arg.edit-part.without-answer')
            ->addCondition(Form::EQUAL, false)
            ->toggle('toggle-answer');
        $form->addText('answer', 'form.treasure-hunt-arg.edit-part.answer')
            ->addConditionOn($form['withoutAnswer'], Form::EQUAL, false)
            ->setRequired('form.treasure-hunt-arg.edit-part.answer.req');

        // BUTTON
        $form->addSubmit('send', 'form.treasure-hunt-arg.edit-part.send');
        $form->addSubmit('send_back', 'form.treasure-hunt-arg.edit-part.send-back');
        $form->addSubmit('back', 'form.treasure-hunt-arg.edit-part.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->treasureHuntArgPart->isLoaded()) {
            return [];
        }

        return [
            'name'          => $this->treasureHuntArgPart->getName(),
            'answer'        => $this->treasureHuntArgPart->getAnswer(),
            'content'       => $this->treasureHuntArgPart->getContent(),
            'withoutAnswer' => $this->treasureHuntArgPart->isWithoutAnswer(),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->treasureHuntArgPart->isLoaded()) {
            $treasureHuntArgPart = $this->facade->update(
                $this->treasureHuntArgPart->getId(),
                $values->name,
                $values->content,
                $values->answer,
                $values->withoutAnswer
            );
            $this->onFlashmessage('form.treasure-hunt-arg.edit-part.flash.success.update', Flash::SUCCESS);
        } else {
            $treasureHuntArg = $this->facadeTreasureHuntArg->get(intval($values->thargId));

            $treasureHuntArgPart = $this->facade->create(
                $treasureHuntArg,
                $values->name,
                $values->content,
                $values->answer,
                $values->withoutAnswer
            );

            $this->treasureHuntArgPart = $treasureHuntArgPart;
            $this->onFlashmessage('form.treasure-hunt-arg.edit-part.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-part',
            'id'      => $treasureHuntArgPart->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-part',
            'id'      => $this->treasureHuntArgPart->getTreasureHuntArg()->getId(),
        ]);
    }
}
