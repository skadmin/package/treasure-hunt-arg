<?php

declare(strict_types=1);

namespace Skadmin\TreasureHuntArg\Components\Admin;

interface IEditPartFactory
{
    public function create(?int $id = null): EditPart;
}
